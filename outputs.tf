/*
Outputs from the 'rds' module
*/

output "rds_cluster_arn" {
  value = aws_db_instance.rds_instance.arn
}

output "rds_instance_dns_name" {
  value = aws_route53_record.rds_instance_dns_record.name
}
