/*
Security groups for the 'rds' module
*/

resource "aws_security_group" "rds_cluster" {
  name        = "${var.app_name}-${var.environment}-rds-cluster-sg"
  description = "Ports needed by the RDS cluster"
  vpc_id      = var.vpc_id

  ingress {
      from_port = 3306
      to_port = 3306
      protocol = "tcp"
      cidr_blocks = var.allowed_cidrs
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}
