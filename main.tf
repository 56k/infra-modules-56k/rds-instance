/*
This module deploys a Mysql RDS instance for the application
*/

# Create a subnet group for the RDS instance
resource "aws_db_subnet_group" "rds" {
  name       = "${var.app_name}-${var.environment}-rds-subnet-group"
  subnet_ids = var.db_subnet_ids

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}

# Create a RDS instance
resource "aws_db_instance" "rds_instance" {
  identifier                      = "${var.app_name}-${var.environment}-db"
  engine                          = "mysql"
  engine_version                  = var.engine_version
  allocated_storage               = var.allocated_storage
  storage_type                    = var.storage_type
  storage_encrypted               = var.storage_encrypted
  instance_class                  = var.instance_class
  name                            = var.db_name
  username                        = var.username
  password                        = var.password
  maintenance_window              = var.maintenance_window
  backup_retention_period         = var.backup_retention_period
  backup_window                   = var.backup_window
  db_subnet_group_name            = aws_db_subnet_group.rds.name
  vpc_security_group_ids          = [aws_security_group.rds_cluster.id]
  publicly_accessible             = var.publicly_accessible
  final_snapshot_identifier       = "${var.app_name}-${var.environment}-db-final-snapshot"
  enabled_cloudwatch_logs_exports = ["error", "general", "slowquery"]

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}

/*
Add a human friendly DNS record that points to the RDS instance endpoint
*/
resource "aws_route53_record" "rds_instance_dns_record" {
    zone_id = var.dns_zone_id
    type    = "CNAME"
    name    = "${var.app_name}-${var.environment}-db"
    ttl     = "300"
    records = [aws_db_instance.rds_instance.address]
}
