/*
Input variables used to configure the 'rds' module
*/

variable "app_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "instance_class" {
  type = string
}

variable "engine" {
  type = string
}

variable "engine_version" {
  type = string
}

variable "username" {
  type = string
}

variable "password" {
  type = string
}

variable "db_name" {
  type = string
}

variable "allocated_storage" {
  type = number
}

variable "storage_type" {
  type = string
}

variable "storage_encrypted" {
  type = bool
}

variable "maintenance_window" {
  type = string
}

variable "backup_retention_period" {
  type = number
}

variable "backup_window" {
  type = string
}

variable "dns_zone_id" {
  type = string
}

variable "publicly_accessible" {
  type = bool
}

variable "availability_zones" {
  type = list
}

variable "db_subnet_ids" {
  type = list
}

variable "vpc_id" {
  type = string
}

variable "allowed_cidrs" {
  type = list
}
